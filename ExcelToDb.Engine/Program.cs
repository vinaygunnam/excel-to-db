﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel;

namespace ExcelToDb.Engine
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var stream = File.Open("C:\\Customers.xlsx", FileMode.Open, FileAccess.Read);
            var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            var result = excelReader.AsDataSet();
            var customers = CustomerHelper.ConvertFromDataTable(result.Tables[0]);
            customers.ForEach(c => Console.WriteLine("{0}: {1} ({2})", c.Id, c.Name, c.Age));

            Console.ReadKey(false);
        }

        internal class Customer
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }

        internal class CustomerHelper
        {
            public static List<Customer> ConvertFromDataTable(DataTable table)
            {
                var customers = new List<Customer>();
                if (table == null || table.Rows == null || table.Rows.Count == 0) return customers;

                foreach (DataRow row in table.Rows)
                {
                    if (row == null || row.ItemArray.Length == 0) continue;
                    try
                    {
                        var customer = new Customer
                        {
                            Id = (int)Convert.ToDouble(row[0]),
                            Age = (int)Convert.ToDouble(row[2]),
                            Name = Convert.ToString(row[1])
                        };
                        customers.Add(customer);
                    }
                    catch (Exception)
                    {
                        // cath the exception
                    }

                }

                return customers;
            }
        }
    }
}